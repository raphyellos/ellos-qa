package com.ellosgroup.funcTestM3;

import java.awt.event.KeyEvent;

import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ScreenRegion;

import com.ellosgroup.funcTestTools.Application;

public class M3Application extends Application{
	
	public M3Application() {
		super("Infor");
	}
	
	public void replaceText(String fieldImage, String text) {
		ScreenRegion r = findImageBounds(fieldImage);
		leftClickImage("input_box.png", new DesktopScreenRegion(r.getX(), r.getY() - 10, 200, 35)); 
		type(text);
	}
	
	public void appendText(String fieldImage, String text) {
		ScreenRegion r = findImageBounds(fieldImage);
		leftClickImage("input_box.png", new DesktopScreenRegion(r.getX(), r.getY() - 10, 200, 35));
		pressKey(KeyEvent.VK_END);
		type(text);
	}
	
	public void prependText(String fieldImage, String text) {
		ScreenRegion r = findImageBounds(fieldImage);
		leftClickImage("input_box.png", new DesktopScreenRegion(r.getX(), r.getY() - 10, 200, 35));
		pressKey(KeyEvent.VK_HOME);
		type(text);
	}
	
	public void setCheckbox(String fieldImage, boolean value) {
		
	}
}
