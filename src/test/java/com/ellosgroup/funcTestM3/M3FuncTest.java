package com.ellosgroup.funcTestM3;

import java.awt.event.KeyEvent;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber", "json:target/cucumber.json" }, 
		monochrome = true, 
		features = "src/test/resources/com/ellosgroup/funcTestM3", 
		glue = "com.ellosgroup.funcTestM3.glue")
public class M3FuncTest {
	
	private static Process process;
	private static M3Application app;
	
	@BeforeClass
	public static void setUp() throws IOException {
		String command = "rundll32.exe dfshim.dll,ShOpenVerbShortcut C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Infor\\Infor Smart Office - InforDT-ISO_Grid_TST.appref-ms";
		System.out.println("Starting SmartOffice");
		process = Runtime.getRuntime().exec(command);
		
		//Start and login to SmartOffice
		app = new M3Application();
		if (!app.waitForImage("smart_office_login.png", 30))
		{
			throw new RuntimeException("Failed to start M3");
		}
		
		//Tab twice to get to user name...
		app.pressKey(KeyEvent.VK_TAB);
		app.pressKey(KeyEvent.VK_TAB);
		app.type("M3TEST1");
		
		// Tab once to get to password...
		app.pressKey(KeyEvent.VK_TAB);
		app.type("InforM3#");
		app.pressKey('\n');
	}
	
	@AfterClass
	public static void tearDown() {
		System.out.println("Closing SmartOffice");
		app.pressKey(KeyEvent.VK_F4, KeyEvent.VK_ALT);
		
		System.out.println("Destroying SmartOffice process");
		process.destroy();
	}
	
}