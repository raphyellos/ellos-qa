package com.ellosgroup.funcTestM3.glue;

import java.awt.event.KeyEvent;

import com.ellosgroup.funcTestM3.M3Application;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {
	private M3Application app;
	
	@Before
	public void initialize() {
		app = new M3Application();
	}
	
	@Given("^I am logged into M3$")
	public void I_am_logged_into_M3() throws Throwable {
	    app.waitForImage("logo_smartoffice.png", 30);
	}

	@When("^I start program '(.*)'$")
	public void I_start_program(String program) throws Throwable {
		app.pressKey(KeyEvent.VK_R, KeyEvent.VK_CONTROL);
		app.type(program);
		app.pressKey('\n');
		
		// TODO: Fix wait for program
		Thread.sleep(2500);
	}

	@Then("^I should see result '(.*)'$")
	public void I_should_see_result(String imageName) throws Throwable {
		if (!app.findImage(imageName))
			throw new RuntimeException("Wrong result!");
	}
	
	@When("^type '(.*)'(?:.*)$")
	public void type(String text) throws Throwable {
	    app.type(text);
	}

	@When("^press '(.*)'(?:.*)$")
	public void press(String key) throws Throwable {
		
	    if (key.equals("Enter")) {
	    	app.pressKey(KeyEvent.VK_ENTER);
	    } else if (key.equals("Esc")) {
	    	app.pressKey(KeyEvent.VK_ESCAPE);
	    } else if (key.equals("Tab")) {
	    	app.pressKey(KeyEvent.VK_TAB);
	    }
	}
	
	@When("^right click '(.*)'(?:.*)$")
	public void right_click(String image) throws Throwable {
	    app.rightClickImage(image);
	}

	@When("^left click '(.*)'(?:.*)$")
	public void left_click(String image) throws Throwable {
		app.leftClickImage(image);
	}
	
	@When("^replace '(.*)' with '(.*)'")
	public void replace(String image, String text) {
		app.replaceText(image, text);
	}
	
	@Then("^close program(?:.*)$")
	public void closeProgram() {
		app.leftClickImage("button_close.png");
	}
}

