@M3
Feature: Search in M3

@TC-900
Scenario: Search for CRS610
	Given I am logged into M3
		When I start program 'CRS610'
		And type 'DOUBLEX' in the search field
		And press 'Enter' to search
		And right click 'doublex_row2.png'
     	And left click 'menu_change.png'
			Then I should see result 'doublex_change.png'
    		And left click 'button_close.png' to close window
     		