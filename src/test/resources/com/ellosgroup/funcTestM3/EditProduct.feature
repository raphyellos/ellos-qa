@M3
Feature: Edit product

  @M3
  Scenario: Edit product ABTTST
   Given I am logged into M3
    When I start program 'MMS001'
     And type 'ABTTST' in the search field
     And press 'Enter' to search
     And right click 'abttst_product_row.png'
     And left click 'menu_change.png'
     And replace 'field_description.png' with 'Demo description'
     And replace 'field_revision_no.png' with '123'
     And left click 'button_next.png' to save the new data
     And left click 'button_back.png' to go back and verify
    Then I should see result 'result_mms001.png'
     And close program